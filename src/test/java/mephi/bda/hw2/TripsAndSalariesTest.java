package mephi.bda.hw2;
import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.junit.Before;
import org.junit.Test;


public class TripsAndSalariesTest {
	
	private static JavaStreamingContext sc;
	//Filesystem type prefix is added for Spark worker method
	private static String TestOutputFilePathWithPrefix = "file:///home/dev/hw2_test_output";
	
	//Filesystem type prefix is omitted for junit test method
	private static String TestOutputFilePathWithOutPrefix = "/home/dev/hw2_test_output";
	
	//Mock stream for junit
	private JavaDStream<String> machineDataDstream;
	private BufferedReader br;

	//initialize mock stream
	@Before
	public void init() throws IOException {
		ArrayList<String> input = new ArrayList<String>();
		//test data with two rows
		input.add("25,100,2");
		input.add("25,300,4");
		SparkConf sparkConf = new SparkConf();
		sparkConf.setMaster("local[2]");
		sparkConf.setAppName("junit");		
		sc = new JavaStreamingContext(sparkConf, new Duration(5000));
		JavaRDD<String> rdd = sc.sparkContext().parallelize(input);
		java.util.Queue<JavaRDD<String>> queue = new LinkedList<JavaRDD<String>>();
		queue.add(rdd);		
		machineDataDstream = sc.queueStream( queue );		
		TripsAndSalaries.outFilePath=TestOutputFilePathWithPrefix;		
	}
	
	//test method - use mock input stream, output to a file on the disk and check the expected aggregated results
	@Test
	public void processTest() throws InterruptedException, Exception {
		TripsAndSalaries.Process(machineDataDstream);
	    sc.start();
	    sc.awaitTerminationOrTimeout(6000);
	    sc.stop();
	    File file = new File(TestOutputFilePathWithOutPrefix + "/part-00000"); 
	    br = new BufferedReader(new FileReader(file)); 
	    String st = br.readLine(); 
	    //expected data
	    assertEquals(st, "20-29, 200.00, 3.00");
	}
}
