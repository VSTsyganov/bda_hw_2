## Task:
Application that calculate average salary and abroad trips count statistics about citizens. Row input format: (passport №, month,  salary) or (passport №, month, number of abroad trips)

* Back-end: MySQL
* Import tool: Sqoop
* Storage: HDFS
* Computation tool: Spark streaming

## Deployment steps:

Clone repository:
```bash
git clone https://VSTsyganov@bitbucket.org/VSTsyganov/bda_hw_2.git
cd bda_hw2
```

Create docker network:
```bash
docker network create hadoop
```

Pull and prepare MySQL docker image:
```bash
docker pull mysql:8.0
docker network create hadoop
docker run --hostname mysql --network hadoop --name mysql -e MYSQL_ROOT_PASSWORD=Orion123 -d mysql:8.0
docker cp scripts/mysql_import_test_data.sh mysql:/opt/
docker exec -it mysql bash
mysql -u root -p < /opt/mysql_import_test_data.sh
exit
```

Pull and prepare Hadoop docker image:
```bash
docker pull vstsyganov/hadoop_hw2:2.0
chmod 777 start-hadoop-container.sh
./scripts/start-hadoop-container.sh
exit
```

Build Spark project:
```bash
mvn clean install
```

Copy jar to docker container:
```bash
docker cp target/hw2-0.0.1-SNAPSHOT.jar hadoop-master:/opt/
```
